import axios from 'axios'
import { Config } from 'App/Config'
import { is, curryN, gte } from 'ramda'
import { Alert } from 'react-native'

const isWithin = curryN(3, (min, max, value) => {
  const isNumber = is(Number)
  return isNumber(min) && isNumber(max) && isNumber(value) && gte(value, min) && gte(max, value)
})
const in200s = isWithin(200, 299)


const topMajorGainerApiClient = axios.create({
  baseURL: Config.API_URL_TOP_MAJOR_GAINER,
  headers: {
    method: 'GET',
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 3000,
})

function fetchTopMajorGainer() {
  return topMajorGainerApiClient.get().then((response) => {
    if (in200s(response.status)) {
      let data = response.data.map((e) => {
        return {
          "symbol": e.symbol,
          "priceChange": e.priceChange,
          "priceChangePercent": e.priceChangePercent,
        }
      })

      return data
    }
    return null
  }).catch((err) => {
    
    return null
  });
}



export const tradingPairsService = {
  fetchTopMajorGainer,
}
