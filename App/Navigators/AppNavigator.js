import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import { Images } from 'App/Theme'
import { createAppContainer } from 'react-navigation'
import TradingPairsScreen from 'App/Containers/TradingPairs/TradingPairsScreen'
import SplashScreen from 'App/Containers/SplashScreen/SplashScreen'
import FavouritesScreen from 'App/Containers/Favourites/FavouritesScreen'
import TopMajorGainerScreen from 'App/Containers/TopMajorGainer/TopMajorGainerScreen'
import {DrawerNavigatorItems} from 'react-navigation-drawer';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack'


const DrawerRouteConfigs = {
  TradingPairs: {
    screen: TradingPairsScreen,
  },
  TopMajorGainer: {
    screen: TopMajorGainerScreen,
  },
  Favourites: {
    screen: FavouritesScreen,
  },
};


const style =  StyleSheet.create({
  drawerHeader: {
    marginTop: Platform.OS === 'ios' ? 40 : 0 ,
    height:100,
  },
  drawerImage:{
    flex:1, width: null, height: null
  }
})

const CustomDrawer = props => {
  return (
    <View>
      <View style={style.drawerHeader}>
        <Image source={Images.binance} resizeMode='cover' style={style.drawerImage} />
      </View>
      <DrawerNavigatorItems {...props} />
    </View>
  );
};

const DrawerNavigatorConfig = {
  contentComponent: CustomDrawer,
  navigationOptions: {
    headerStyle: {
      backgroundColor: '#f4511e',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      color: 'white',
    },
  },
  contentOptions: {
    activeTintColor: '#e91e63',
    inactiveTintColor: '#f0b909',
    itemsContainerStyle: {
      marginVertical: 0,
    },
    iconContainerStyle: {
      opacity: 1,
    },
  },
  drawerBackgroundColor: '#262A2C', 
};

const StackDrawer = createDrawerNavigator(DrawerRouteConfigs, DrawerNavigatorConfig);

const StackNavigator = createStackNavigator(
  {
    SplashScreen: SplashScreen,
    MainScreen: StackDrawer,
  },
  {
    initialRouteName: 'SplashScreen',
    headerMode: 'none',
  }
)


export default createAppContainer(StackNavigator)


