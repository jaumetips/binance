import { put, call } from 'redux-saga/effects'
import ExampleActions from 'App/Stores/Example/Actions'
import { tradingPairsService } from 'App/Services/TradingPairsService'

/**
 * A saga can contain multiple functions.
 *
 */

export function* fetchTopMajorGainer() {
  // Dispatch a redux action using `put()`
  // @see https://redux-saga.js.org/docs/basics/DispatchingActions.html
  yield put(ExampleActions.fetchTopMajorGainerLoading())
  // Fetch TopMajorGainer informations from an API
  const topMajorGainer = yield call(tradingPairsService.fetchTopMajorGainer)

  if (topMajorGainer) {
    yield put(ExampleActions.fetchTopMajorGainerSuccess(topMajorGainer))
  } else {
    yield put(ExampleActions.fetchTopMajorGainerFailure('There was an error while fetching Trading informations.')
    )
  }
}


