import { takeLatest, all } from 'redux-saga/effects'
import { ExampleTypes } from 'App/Stores/Example/Actions'
import { StartupTypes } from 'App/Stores/Startup/Actions'
import { fetchTopMajorGainer} from './ExampleSaga'
import { startup } from './StartupSaga'

export default function* root() {
  yield all([
    /**
     * @see https://redux-saga.js.org/docs/basics/UsingSagaHelpers.html
     */
    // Run the startup saga when the application starts
    takeLatest(StartupTypes.STARTUP, startup),

    // Call `fetchTopMajorGainer()` when a `FETCH_TOP_MAJOR_GAINER` action is triggered
    takeLatest(ExampleTypes.FETCH_TOP_MAJOR_GAINER, fetchTopMajorGainer),

  ])
}
