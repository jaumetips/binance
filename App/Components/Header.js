import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { Helpers, Metrics, Colors} from 'App/Theme'
import { Icon } from 'react-native-elements';
import Style from './HeaderStyle'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'

class Header extends React.Component {

  render() {
    return (
      <TouchableOpacity onPress={()=> this.props.navigation.openDrawer()} activeOpacity={1}>
        <View>
          <View style={[
            Helpers.header,
            Helpers.fullWidth,           
            Metrics.horizontalPadding,
            Metrics.verticalPadding,
            Helpers.row,
            Style.backgroundColor]}>
            <Icon
              name='menu'
              type='material-community'
              color={Colors.yellow}
              containerStyle = {[Helpers.crossStart, Metrics.marginRight]}
               />
            <Text style={[Style.text]}>{this.props.title}</Text>
          </View>
          {!this.props.network && 
            <View style={[Helpers.fullWidth,           
              Metrics.tinyVerticalPadding,
              Helpers.center,
              Helpers.row,
              Style.backgroundColorAlert]}>
              <Text style={[ Style.text]}>Offline</Text>
            </View>
          }
        </View>  
      </TouchableOpacity>
    )
  }
}


Header.propTypes = {
  network: PropTypes.bool,
}

const mapStateToProps = (state) => ({
  network: state.example.network,
})

const mapDispatchToProps = (dispatch) => ({})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header)



