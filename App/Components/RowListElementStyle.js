import { StyleSheet } from 'react-native'
import { Metrics, Fonts, Colors } from 'App/Theme'

export default StyleSheet.create({
  text: {
    ...Fonts.normal,
    color:Colors.white,
    fontWeight: 'bold',
  },
  textGreen: {
    ...Fonts.normal,
    color:Colors.success,
    fontWeight: 'bold',
  },
  textRed: {
    ...Fonts.normal,
    color:Colors.red,
    fontWeight: 'bold',
  }
})
