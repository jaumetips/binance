import React, {PureComponent} from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import ExampleActions from 'App/Stores/Example/Actions'
import { Helpers, Metrics, Colors } from 'App/Theme'
import { Icon } from 'react-native-elements';
import { PropTypes } from 'prop-types'
import { connect } from 'react-redux'
import Style from './RowListElementStyle'


class RowListElement extends PureComponent {

  constructor(props) {
    super(props)
    this.state = {
        item: {},
      }
    }

  componentDidMount() {
    this.displayFavourites();
  }

  displayFavourites(){
    let item = this.props.item;
    let fav = this.props.favouritesPairs.find(fav => fav.symbol == item.symbol);
    if(fav){
        item.fav = true;
        this.setState({item: item});
    }
  }
  saveFavourite=()=>{
    let item = {
      ...this.props.item,
      fav: !this.props.item.fav
    }
    this._savefavouritesPairs(item);
  }
  _savefavouritesPairs(item){
    item.fav ? this.props.saveFavouritesPairs(item) : this.props.deleteFavouritesPairs(item)
  }
  detectPositiveVariation=()=> {
    let item = this.props.item;
    return item.priceChange > 0 ? 'arrow-up' : 'arrow-down'
  }
  detectVariationColor=()=> {
    let num = this.props.item.priceChange;
    let color = (num == 0) ? Colors.black : (num < 0) ? Colors.red : Colors.success;
    return color;
  }

  render() {
    const {item, displayPriceChangePercent, displayPriceChange} = this.props;
    return (
      <TouchableOpacity onPress={()=> this.saveFavourite(item)}>
        <View key={item.symbol} style={[Helpers.fillRowMain, Metrics.verticalMargin, Metrics.horizontalMargin]}>
          <Icon
            name={this.detectPositiveVariation()}
            type='material-community'
            color={this.detectVariationColor()}
            containerStyle={[Metrics.smallHorizontalMargin]}
          />
          <Text style={[Style.text ]}>{item.symbol}</Text>
          <View style={[Helpers.fill , Helpers.crossEnd, Style.text, Metrics.horizontalMargin]}>
            {displayPriceChangePercent && 
              <Text style={[Style.text]}>{item.priceChangePercent}%</Text>
            }
            {displayPriceChange && 
              <Text style={[Style.text]}>{item.priceChange}</Text>
            }
          </View>
          <Icon
            name={item.fav ? 'star' : 'star-outline'}
            type='material-community'
            color={Colors.yellow}
          />
        </View>
      </TouchableOpacity>
    )
  }
}


RowListElement.propTypes = {
  favouritesPairs:PropTypes.array,
  saveFavouritesPairs: PropTypes.func,
  deleteFavouritesPairs: PropTypes.func,
}

const mapStateToProps = (state) => ({
  favouritesPairs:state.example.favouritesPairs,
})

const mapDispatchToProps = (dispatch) => ({
  saveFavouritesPairs: (favouritesPairs) => dispatch(ExampleActions.saveFavouritesPairs(favouritesPairs)),
  deleteFavouritesPairs: (favouritesPairs) => dispatch(ExampleActions.deleteFavouritesPairs(favouritesPairs)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RowListElement)



