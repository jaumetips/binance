import { StyleSheet } from 'react-native'
import {Metrics, Fonts, Colors } from 'App/Theme'

export default StyleSheet.create({
  text: {
    ...Fonts.normal,
    color:Colors.yellow,
    marginBottom: Metrics.tiny,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  backgroundColor:{
    backgroundColor:Colors.headerColor
  },
  backgroundColorAlert:{
    backgroundColor:Colors.red
  }


  
})
