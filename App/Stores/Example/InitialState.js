/**
 * The initial values for the redux state.
 */
export const INITIAL_STATE = {

  topMajorGainer: [] ,
  topMajorGainerIsLoading: false,
  topMajorGainerErrorMessage: null,
  favouritesPairs: [],
  network: false,
}
