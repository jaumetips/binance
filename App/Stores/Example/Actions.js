import { createActions } from 'reduxsauce'

/**
 * We use reduxsauce's `createActions()` helper to easily create redux actions.
 *
 * Keys are action names and values are the list of parameters for the given action.
 *
 * Action names are turned to SNAKE_CASE into the `Types` variable. This can be used
 * to listen to actions:
 *
 * - to trigger reducers to update the state, for example in App/Stores/Example/Reducers.js
 * - to trigger sagas, for example in App/Sagas/index.js
 *
 * Actions can be dispatched:
 *
 * - in React components using `dispatch(...)`, for example in App/App.js
 * - in sagas using `yield put(...)`, for example in App/Sagas/ExampleSaga.js
 *
 * @see https://github.com/infinitered/reduxsauce#createactions
 */

const { Types, Creators } = createActions({

  // Fetch TopMajorGainer informations
  fetchTopMajorGainer: null,
  // The operation has started and is loading
  fetchTopMajorGainerLoading: null,
  // User informations were successfully fetched
  fetchTopMajorGainerSuccess: ['topMajorGainer'],
  // An error occurred
  fetchTopMajorGainerFailure: ['errorMessage'],
  //Save to favourites a Trading Pair
  saveFavouritesPairs: ['favouritesPairs'],
  //Delete to favourites a Trading Pair
  deleteFavouritesPairs: ['favouritesPairs'],
  //Set Network Status if device connection in online or offline
  setNetworkStatus:['network'],
 
})

export const ExampleTypes = Types
export default Creators
