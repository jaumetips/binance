/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState'
import { createReducer } from 'reduxsauce'
import { ExampleTypes } from './Actions'


export const fetchTopMajorGainerLoading = (state) => ({
  ...state,
  topMajorGainerIsLoading: true,
  topMajorGainerErrorMessage: null,
})

export const fetchTopMajorGainerSuccess = (state, { topMajorGainer }) => ({
  ...state,
  topMajorGainer: topMajorGainer,
  topMajorGainerIsLoading: false,
  topMajorGainerErrorMessage: null,
})

export const fetchTopMajorGainerFailure = (state, { errorMessage }) => ({
  ...state,
  topMajorGainerIsLoading: false,
  topMajorGainerErrorMessage: errorMessage,
})


export const saveFavouritesPairs = (state, { favouritesPairs }) => {
  const topMajorGainer = state.topMajorGainer.map(item => {
    if(item.symbol == favouritesPairs.symbol){
      item.fav = true;
    }
    return item;
  });
  return  {
    ...state,
    favouritesPairs: [...state.favouritesPairs, favouritesPairs],
    topMajorGainer:topMajorGainer

  }
}


export const deleteFavouritesPairs = (state, { favouritesPairs }) => {
  const topMajorGainer = state.topMajorGainer.map(item => {
    if(item.symbol == favouritesPairs.symbol){
      item.fav = false;
    }
    return item;
  });
  return  {
    ...state,
    favouritesPairs: state.favouritesPairs.filter(item => item.symbol !== favouritesPairs.symbol),
    topMajorGainer:topMajorGainer
  }
}


export const setNetworkStatus = (state, { network }) => ({
  ...state,
  network: network
})















/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {

  [ExampleTypes.FETCH_TOP_MAJOR_GAINER_LOADING]: fetchTopMajorGainerLoading,
  [ExampleTypes.FETCH_TOP_MAJOR_GAINER_SUCCESS]: fetchTopMajorGainerSuccess,
  [ExampleTypes.FETCH_TOP_MAJOR_GAINER_FAILURE]: fetchTopMajorGainerFailure,


  [ExampleTypes.SAVE_FAVOURITES_PAIRS]: saveFavouritesPairs,
  [ExampleTypes.DELETE_FAVOURITES_PAIRS]: deleteFavouritesPairs,

  [ExampleTypes.SET_NETWORK_STATUS]: setNetworkStatus,


})
