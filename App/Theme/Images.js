/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {
  binance: require('App/Assets/Images/binance.jpg'),
}
