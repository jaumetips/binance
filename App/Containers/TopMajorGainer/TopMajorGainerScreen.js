import React from 'react'
import { View, ActivityIndicator, FlatList, RefreshControl } from 'react-native'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import ExampleActions from 'App/Stores/Example/Actions'
import { Helpers, Colors } from 'App/Theme'
import Header from '../../Components/Header'
import Style from './TopMajorGainerScreenStyle'
import RowListElement from '../../Components/RowListElement'


class TopMajorGainerScreen extends React.Component {

  sortBy_priceChangePercent(data){
    return data.concat().sort((a,b) => b.priceChangePercent - a.priceChangePercent);
  }
  _onRefresh = () => {
    this.props.fetchTopMajorGainer()
  }

  render() {
    return (
      <View style={[Helpers.fill, Style.backgroundColor]}>
        <Header title='Top Major Gainer' navigation={this.props.navigation}/>

        {this.props.topMajorGainerIsLoading ? (
        <ActivityIndicator style = {[Helpers.fill, Helpers.center]} size="large" color={Colors.yellow} />
        ) : (
            <FlatList
              data={this.sortBy_priceChangePercent(this.props.topMajorGainer)}
              maxToRenderPerBatch={10}
              updateCellsBatchingPeriod={200}
              keyExtractor={item => item.symbol}
              onEndReachedThreshold={0.5}
              initialNumToRender={12}
              windowSize={4}
              renderItem={({ item }) => (
                <RowListElement item={item} displayPriceChangePercent={true}/>
              )}
              refreshControl={
                <RefreshControl
                  refreshing={this.props.topMajorGainerIsLoading}
                  onRefresh={this._onRefresh}
                />
              }/> 
        )}
      </View>
    )
  }
}


TopMajorGainerScreen.propTypes = {
  topMajorGainer: PropTypes.array,
  topMajorGainerIsLoading: PropTypes.bool,
}

const mapStateToProps = (state) => ({
  topMajorGainer: state.example.topMajorGainer,
  topMajorGainerIsLoading: state.example.topMajorGainerIsLoading,
})

const mapDispatchToProps = (dispatch) => ({
  fetchTopMajorGainer: () => dispatch(ExampleActions.fetchTopMajorGainer()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TopMajorGainerScreen)
