import { StyleSheet } from 'react-native'
import { Colors, Fonts, Metrics } from 'App/Theme'

export default StyleSheet.create({
  backgroundColor:{
    backgroundColor:Colors.black
  },
  text: {
    ...Fonts.normal,
    color:Colors.white,
    opacity:0.2,
    fontWeight: 'bold',
  },

})
