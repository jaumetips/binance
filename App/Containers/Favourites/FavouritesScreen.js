import React from 'react'
import { View, FlatList, Text} from 'react-native'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import { Helpers} from 'App/Theme'
import Header from '../../Components/Header'
import RowListElement from '../../Components/RowListElement'
import Style from './FavouritesScreenStyle'

class FavouritesScreen extends React.Component {

  render() {
    return (
      <View style={[Helpers.fill, Style.backgroundColor]}>
        <Header title='Favourites' navigation={this.props.navigation}/>
            {this.props.favouritesPairs.length == 0 ?
            <View style={[Helpers.fill, Helpers.center ]}>
              <Text style={[Style.text]}>No favourites</Text> 
            </View>
            :
            <FlatList
              data={this.props.favouritesPairs}
              maxToRenderPerBatch={10}
              updateCellsBatchingPeriod={200}
              keyExtractor={item => item.symbol}
              onEndReachedThreshold={0.5}
              initialNumToRender={12}
              windowSize={4}
              renderItem={({ item }) => (
                <RowListElement item={item} displayPriceChangePercent={true} displayPriceChange={true}/>
              )}
            />
          }
      </View>
    )
  }
}


FavouritesScreen.propTypes = {
  favouritesPairs:PropTypes.array,
}

const mapStateToProps = (state) => ({
  favouritesPairs: state.example.favouritesPairs,
})

const mapDispatchToProps = (dispatch) => ({
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FavouritesScreen)
