import React from 'react'
import { View, StatusBar } from 'react-native'
import styles from './SplashScreenStyle'
import { Helpers, Colors } from 'App/Theme'

export default class SplashScreen extends React.Component {

  render() {
    return (
      <View style={[Helpers.fillRowCenter, styles.container]}>
        <StatusBar backgroundColor={Colors.black} barStyle="light-content"/>
        <View style={[Helpers.center, styles.logo]}>
        </View>
      </View>
    )
  }
}
