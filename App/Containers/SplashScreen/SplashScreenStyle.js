import { StyleSheet } from 'react-native'
import { Colors } from 'App/Theme'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.black,
  },
  logo: {
    backgroundColor: Colors.black,
    height: 70,
    width: 70,
  },
})
