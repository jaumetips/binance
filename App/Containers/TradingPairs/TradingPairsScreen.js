import React from 'react'
import { View, ActivityIndicator, FlatList, StatusBar, RefreshControl } from 'react-native'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import ExampleActions from 'App/Stores/Example/Actions'
import Style from './TradingPairsScreenStyle'
import { Helpers, Colors } from 'App/Theme'
import Header from '../../Components/Header'
import RowListElement from '../../Components/RowListElement'

class TradingPairsScreen extends React.Component {

  _onRefresh = () => {
    this.props.fetchTopMajorGainer()
  }

  render() {
    return (
      <View style={[Helpers.fill, Style.backgroundColor]}>
        <StatusBar backgroundColor={Colors.headerColor} barStyle="light-content"/>
        <Header title='Trading Pairs' navigation={this.props.navigation}/>

        {this.props.topMajorGainerIsLoading ? (
        <ActivityIndicator style = {[Helpers.fill, Helpers.center]} size="large" color={Colors.yellow} />
        ) : (
            <FlatList
              maxToRenderPerBatch={10}
              updateCellsBatchingPeriod={200}
              data={this.props.topMajorGainer}
              keyExtractor={item => item.symbol}
              onEndReachedThreshold={0.5}
              initialNumToRender={12}
              windowSize={4}
              renderItem={({ item }) => (
                <RowListElement item={item} displayPriceChange={true}/>
              )}
              refreshControl={
                <RefreshControl
                  refreshing={this.props.topMajorGainerIsLoading}
                  onRefresh={this._onRefresh}
                />
              }/> 
        )}
      </View>
    )
  }
}


TradingPairsScreen.propTypes = {
  topMajorGainer: PropTypes.array,
  topMajorGainerIsLoading: PropTypes.bool,
}

const mapStateToProps = (state) => ({
  topMajorGainer: state.example.topMajorGainer,
  topMajorGainerIsLoading: state.example.topMajorGainerIsLoading,
})

const mapDispatchToProps = (dispatch) => ({
  fetchTopMajorGainer: () => dispatch(ExampleActions.fetchTopMajorGainer()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TradingPairsScreen)
