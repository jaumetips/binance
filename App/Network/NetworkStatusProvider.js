import React from 'react'
import ExampleActions from 'App/Stores/Example/Actions'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import NetInfo from "@react-native-community/netinfo";


class NetworkStatusProvider extends React.Component {
    componentDidMount() {
        NetInfo.addEventListener(state => {
            this.props.setNetworkStatus(state.isConnected);
        });
    }
    render() {
        return this.props.children;
    }
}



NetworkStatusProvider.propTypes = {
  setNetworkStatus: PropTypes.func,
  network: PropTypes.bool,
}

const mapStateToProps = (state) => ({
  network: state.example.network,
})

const mapDispatchToProps = (dispatch) => ({
  setNetworkStatus: (network) => dispatch(ExampleActions.setNetworkStatus(network)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NetworkStatusProvider)





